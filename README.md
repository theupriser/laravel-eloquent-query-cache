Laravel Eloquent Query Cache
============================

Laravel Eloquent Query Cache brings back the `remember()` functionality that has been removed from Laravel a long time ago.
It adds caching functionalities directly on the Eloquent level, making use of cache within your database queries.

## 📃 Documentation

[The entire documentation is available on Gitbook 🌍](https://laravel-eloquent-query-cache-docs-50d4e3.gitlab.io/)

## 🤝 Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## 🔒  Security

If you discover any security related issues, please email rickpeters@upriser.nl instead of using the issue tracker.
